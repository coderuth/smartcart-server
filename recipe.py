import pymysql
import logging, traceback
import random
from poll_db import Message
msg = Message()
class Recipe(object):
    def __init__(self):
        self.connection = pymysql.connect(
            host = 'localhost',
            user = 'root',
            password = '',
            db = 'smartcart'
        )
    
    def randN(self, n):
        assert n <= 10
        l = list(range(10)) # compat py2 & py3
        while l[0] == 0:
            random.shuffle(l)
        return int(''.join(str(d) for d in l[:n]))

    def get_recipe(self, name):
        temp = []
        try:
            with self.connection.cursor() as cursor:
                sql = "SELECT * FROM recipe_index WHERE name LIKE %s;"
                cursor.execute(sql, "%" + name + "%")
                result = cursor.fetchall()
                for row in result:
                    temp.append({ 'id': row[0], 'name': row[1], 'image': row[2] })
                return temp
            self.connection.commit()
        except:
            print("Some Error Occured")
    
    def get_all_recipes(self):
        temp = []
        try:
            with self.connection.cursor() as cursor:
                sql = "SELECT * FROM recipe_index"
                cursor.execute(sql)
                result = cursor.fetchall()
                for row in result:
                    temp.append({ 'id': row[0], 'name': row[1], 'image': row[2] })
                return {'recipe': temp}
            self.connection.commit()
        except:
            print("Some Error Occured")
    
    def login(self, cart_id, user_id, password):
        try:
            with self.connection.cursor() as cursor:
                sql = "INSERT INTO cart_index VALUES(%s)"
                cursor.execute(sql, cart_id)
            self.connection.commit()
        except:
            print("Some Error Occured1")

        try:
            with self.connection.cursor() as cursor:
                sql = "SELECT username, password FROM user where username = %s"
                cursor.execute(sql, user_id)
                res = cursor.fetchone()
                self.connection.commit()
                if res[1] == password:
                    try:
                        with self.connection.cursor() as cursorone:
                            sql = "UPDATE user SET cart_id = %s WHERE username = %s"
                            cursorone.execute(sql, (cart_id, user_id))
                        self.connection.commit()
                        return { 'message': "success" }
                    except Exception as e:
                        logging.error(traceback.format_exc())
                        print("Some Error Occured2")
                else:
                    { 'message': "failure" }
        except Exception as e:
            logging.error(traceback.format_exc())
            print("Some Error Occured 3")

    def register(self, username, password):
        try:
            with self.connection.cursor() as cursor:
                sql = "INSERT INTO user VALUES(%s,%s,%s, DEFAULT)"
                cursor.execute(sql, (self.randN(4), username, password))
            self.connection.commit()
            return { 'message' : "user added" }
        except Exception as e:
            logging.error(traceback.format_exc())
            print("UID Exists.")

    def get_items(self, id):
        temp = []
        try:
            with self.connection.cursor() as cursor:
                sql = "SELECT * FROM recipe WHERE rid = %s"
                cursor.execute(sql, id)
                result = cursor.fetchall()
                for row in result:
                    temp.append({ 'item_id': row[1], 'quantity': row[2] })
                return { 'id': row[0], 'recipe': temp }
            self.connection.commit()
        except:
            print("Some Error Occured")

    def add_item_to_cart(self, item_id, cart_id, qty):
        try:
            with self.connection.cursor() as cursor:
                sql = "INSERT INTO cart VALUES(%s, %s, %s)"
                cursor.execute(sql, (cart_id, item_id, qty))
                self.connection.commit()    
                upd = "UPDATE items set stock = stock - %s where id = %s"
                cursor.execute(upd, (qty, item_id))
            self.connection.commit()
            msg.run()
            return { 'message': 'added item to cart' }
        except Exception as e:
            logging.error(traceback.format_exc())
            print("Some Error Occured")

    def get_item(self, item_id):
        try:
            with self.connection.cursor() as cursor:
                sql = "SELECT * FROM items WHERE id = %s"
                cursor.execute(sql, item_id)
                result = cursor.fetchone()
                return { 'id': result[0], 'location_id': result[1], 'name': result[2], 'price': result[3], 'stock': result[4] }
            self.connection.commit()
        except:
            print("Some Error Occured")
        
    def add_item(self, item_id, cart_id):
        try:
            with self.connection.cursor() as cursor:
                sql = "SELECT * FROM location WHERE lid = %s"
                cursor.execute(sql, cart_id)
                result = cursor.fetchone()
                return { 'id': result[0], 'x': result[1], 'y': result[2] }
            self.connection.commit()
        except:
            print("Some Error Occured")

    def item_location(self, item_id):
        try:
            with self.connection.cursor() as cursor:
                sql = "SELECT * FROM location WHERE lid = %s"
                cursor.execute(sql, item_id)
                result = cursor.fetchone()
                return { 'id': result[0], 'x': int(result[1]), 'y': int(result[2]) }
            self.connection.commit()
        except:
            print("Some Error Occured")

    def checkout(self, cart_id):
        total = 0
        bill_item  = []
        try:
            with self.connection.cursor() as cursor:
                sql = "SELECT * FROM cart WHERE cart_id = %s"
                cursor.execute(sql, cart_id)
                result = cursor.fetchall()
                for row in result:
                    temp = self.get_item(row[1])
                    bill_item.append({ 
                        'item_id': row[1], 
                        'quantity': row[2], 
                        'name': temp['name'], 
                        'price': temp['price'] 
                    })
                    total = total + (int(temp['price']) * row[2])
                return { 'total': total, 'items': bill_item }
            self.connection.commit()
        except:
            print("Some Error Occured")

    def order(self, user_id, cart_id):
        order_id = self.randN(8)
        try:
            with self.connection.cursor() as cursor:
                sql = "INSERT INTO `order` VALUES(%s, %s, %s, 0)"
                us = "SELECT uid from user where username = %s"
                cursor.execute(us, user_id)
                res = cursor.fetchone()
                self.connection.commit()
                cursor.execute(sql, (order_id, res[0], int(cart_id)))
            self.connection.commit()
            return { 
                'message' : "order placed",
                'id': order_id 
            }
        except Exception as e:
            logging.error(traceback.format_exc())
            print("UID Exists.")

    def payment(self, order_id):
        try:
            with self.connection.cursor() as cursor:
                get = "SELECT cart_id FROM `order` where id = %s"
                cursor.execute(get, order_id)
                res = cursor.fetchone()
                
                sql = "UPDATE `order` SET payment = 1 where id = %s"
                cursor.execute(sql, order_id)
                
                sql1 = "DELETE FROM cart_index WHERE cartid = %s"
                cursor.execute(sql1, res[0])
            self.connection.commit()
            return { 'message' : "paid for the order" }
        except Exception as e:
            logging.error(traceback.format_exc())
            print("Payment Failed")



