from flask import Flask, request
import json
app = Flask(__name__)
from recipe import Recipe

rec = Recipe()

# @app.route('/search/recipes', methods=['GET', 'POST'])
# def index():
# 	print(str(json.loads(request.data)['query']))
# 	data = rec.get_recipe(str(json.loads(request.data)['query']))
# 	print(data)
# 	response = app.response_class(response=json.dumps(data), status=200, mimetype='application/json')
# 	return response

@app.route('/recipes', methods=['GET'])
def get_all():
	data = rec.get_all_recipes()
	# print(data)
	response = app.response_class(response=json.dumps(data), status=200, mimetype='application/json')
	return response

@app.route('/getitems', methods=['GET'])
def get_items():
	name = request.args.get('recipe_id')
	data = rec.get_items(name)
	response = app.response_class(response=json.dumps(data), status=200, mimetype='application/json')
	return response
	
@app.route('/register',methods=['GET'])
def register():
	uid = request.args.get('u_id')
	password = request.args.get('pass')
	data = rec.register(uid, password)
	response = app.response_class(response=json.dumps(data), status=200, mimetype='application/json')
	return response

@app.route('/login', methods=['GET'])
def login():
	uid = request.args.get('u_id')
	password = request.args.get('pass')
	cart = request.args.get('cart')
	data = rec.login(cart, uid, password)
	response = app.response_class(response=json.dumps(data), status=200, mimetype='application/json')
	return response

@app.route('/getitem', methods=['GET'])
def get_item():
	id = request.args.get('item_id')
	data = rec.get_item(id)
	response = app.response_class(response=json.dumps(data), status=200, mimetype='application/json')
	return response

@app.route('/add_item', methods=['GET'])
def add_item():
	id = request.args.get('item_id')
	cart_id = request.args.get('cart_id')
	qty = request.args.get('qty')
	data = rec.add_item_to_cart(id, cart_id, qty)
	response = app.response_class(response=json.dumps(data), status=200, mimetype='application/json')
	return response

@app.route('/item_location', methods=['GET'])
def get_location():
	id = request.args.get('location')
	data = rec.item_location(id)
	response = app.response_class(response=json.dumps(data), status=200, mimetype='application/json')
	return response

@app.route('/checkout', methods=['GET'])
def checkout():
	id = request.args.get('cart_id')
	data = rec.checkout(id)
	response = app.response_class(response=json.dumps(data), status=200, mimetype='application/json')
	return response

@app.route('/order', methods=['GET'])
def order():
	user_id = request.args.get('user_id')
	cart_id = request.args.get('cart_id')
	data = rec.order(user_id, cart_id)
	response = app.response_class(response=json.dumps(data), status=200, mimetype='application/json')
	return response

@app.route('/payment', methods=['GET'])
def payment():
	id = request.args.get('order_id')
	data = rec.payment(id)
	response = app.response_class(response=json.dumps(data), status=200, mimetype='application/json')
	return response


@app.route('/')
def root():
	return "Yo, it's working!"

if __name__ == "__main__":
	app.run(host='0.0.0.0',port=5000, debug=True)
