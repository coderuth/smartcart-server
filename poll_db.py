import pymysql
import logging, traceback
import random
from twilio.rest import Client
import pymysql
import time
import threading

class Message(threading.Thread):
    account_sid = "AC99370aac59d63826fa665faaca441fe8"
    auth_token = "1013d004afc2739f71aff66b53fae11e"
    client = Client(account_sid, auth_token)
    connection = pymysql.connect(
        host = 'localhost',
        user = 'root',
        password = '',
        db = 'smartcart'
    )
    def get_item_string(self, items):
        string = ""
        for x in items:
            string = string + str(x[0]) + ' - ' + str(x[1]) + '\n'
        return string     

    def run(self):
      print "sent stock status"
      self.send_message()

    def send_message(self):
        res = ()
        try:
            with self.connection.cursor() as cursor:
                sql = "SELECT id, name, avail from items where avail = 1"
                cursor.execute(sql)
                res = cursor.fetchall()
                st = str(self.get_item_string(res))
            self.connection.commit()
            self.client.api.account.messages.create(
                to="+918970464490",
                from_="+14158437470",
                body=st
            )
        except Exception as e:
            logging.error(traceback.format_exc())
            print("Error Occured")

if __name__ == "__main__":
  msg = Message()
  msg.run()
